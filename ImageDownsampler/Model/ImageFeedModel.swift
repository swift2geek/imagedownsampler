//
//  FeedModel.swift
//  ImageDownsampler
//
//  Created by Vladimir Valter on 08.08.19.
//  Copyright © 2019 Vladimir Valter. All rights reserved.
//

import UIKit

/// The model to keep the information about downsampled image.
struct ImageFeedModel {

    // MARK: Properties

    /// The downsampled image.
    var image: UIImage?

    /// The `URL` to local stored image file.
    var url: URL

    /// The model identifier.
    var id: UUID

    init(id: UUID, url: URL) {
        self.url = url
        self.id = id
    }
}
