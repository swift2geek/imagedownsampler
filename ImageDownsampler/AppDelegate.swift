//
//  AppDelegate.swift
//  ImageDownsampler
//
//  Created by Vladimir Valter on 06.08.19.
//  Copyright © 2019 Vladimir Valter. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

