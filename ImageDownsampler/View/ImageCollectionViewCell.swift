//
//  ImageCollectionViewCell.swift
//  ImageDownsampler
//
//  Created by Vladimir Valter on 09.08.19.
//  Copyright © 2019 Vladimir Valter. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell, IReusableView {

    // MARK: Properties

    /// `UIImageView` to display an image.
    @IBOutlet weak var previewImageView: UIImageView!

    /// The `UUID` for connect with async processings.
    var representedId: UUID?

    /// Configures the cell for display the `UIImage`.
    ///
    /// - Parameter image: The `UIImage` to display or nil.
    func update(with image: UIImage?) {
        previewImageView.image = image
    }
}
