//
//  ReusableView.swift
//  ImageDownsampler
//
//  Created by Vladimir Valter on 09.08.19.
//  Copyright © 2019 Vladimir Valter. All rights reserved.
//

import UIKit

/// Protocol for views which may be reusable.
protocol IReusableView: AnyObject {

    /// A string used to identify a `ReusableView` that is reusable.
    static var defaultReuseIdentifier: String { get }
}

extension IReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

extension UICollectionView {
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T where T: IReusableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }

        return cell
    }
}
