//
//  UIView+Nib.swift
//  ImageDownsampler
//
//  Created by Vladimir Valter on 08.08.19.
//  Copyright © 2019 Vladimir Valter. All rights reserved.
//

import UIKit

public protocol NibLoadable: class {
    static var nib: UINib { get }
}

extension NibLoadable where Self: UIView {
    public static var nib: UINib {
        let nibName = String(describing: self)
        let nibBundle = Bundle(for: self)
        return UINib(nibName: nibName, bundle: nibBundle)
    }
}

// MARK: - Reusable
public protocol NibOwnerLoadable: class {
    static var nib: UINib { get }
}

public extension NibOwnerLoadable {
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
}
public extension NibOwnerLoadable where Self: UIView {
    func loadNibContent() {
        let layoutAttributes: [NSLayoutConstraint.Attribute] = [.top, .leading, .bottom, .trailing]
        for view in Self.nib.instantiate(withOwner: self, options: nil) {
            if let view = view as? UIView {
                view.translatesAutoresizingMaskIntoConstraints = false
                self.addSubview(view)
                layoutAttributes.forEach { attribute in
                    self.addConstraint(NSLayoutConstraint(item: view,
                                                          attribute: attribute,
                                                          relatedBy: .equal,
                                                          toItem: self,
                                                          attribute: attribute,
                                                          multiplier: 1,
                                                          constant: 0.0))
                }
            }
        }
    }
}

